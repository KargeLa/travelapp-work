
import UIKit

class ViewControllerWithLabel : UIViewController {
    
    
    @IBOutlet weak var labelOutlet: UILabel!
    
    var labelText = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelOutlet.text = labelText 
    }
    
    
}
