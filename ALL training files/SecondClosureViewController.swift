
import UIKit

class SecondClosureViewController: UIViewController {
    var newText : String = ""
    var transferFromFirstVC: ((String) -> Void)?
    
    //    MARK: - Outlets
    @IBOutlet weak var secondLabel: UILabel!
    @IBOutlet weak var secondTextField: UITextField!
    
    //    MARK: - Actions
    
    @IBAction func secondButtonCliced(_ sender: Any) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        secondLabel.text = newText
        
    }
    
}


