
import UIKit

class BackViewController: UIViewController {
    var goText : String = ""
    var delegate : GoViewController?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backLabelOutlet.text = goText
        
    }
    //    MARK: - Outlet
    @IBOutlet weak var backLabelOutlet: UILabel!
    @IBOutlet weak var backTextFieldOutlet: UITextField!
    //    MARK: - Action
    
    @IBAction func backButtonClicked(_ sender: UIButton) {
        delegate?.changeTextOnGoLabel(String(backTextFieldOutlet.text!))
        navigationController?.popViewController(animated: true)
    }
    
}
