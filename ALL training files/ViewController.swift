import UIKit

class ViewController: UIViewController {
    
    //MARK: - Outlets
    
    @IBOutlet weak var stepperOutlet: UIStepper!
    @IBOutlet weak var labelOutlet: UILabel!
    @IBOutlet weak var sliderLabel: UILabel!
    
    
    //MARK: - Actions
    
    @IBAction func sliderClicked(_ sender: UISlider) {
        sliderLabel.isHidden = false
        sliderLabel.text = String(Int(sender.value))
    }
    
    @IBAction func switchAction(_ sender: UISwitch) {
        if (sender.isOn == true) {
            if let text = labelOutlet.text, let intText = Int(text) {
                if intText > 0 {
                    let result = intText * (-1)
                    labelOutlet.text = String(result)
                }
                
            }
        } else if (sender.isOn == false) {
            if let text = labelOutlet.text, let intText = Int(text) {
                if intText > 0 {
                    let result = -(intText)
                    labelOutlet.text = String(result)
                } else if intText < 0 {
                    let result = intText
                    labelOutlet.text = String(result)
                }

            }
            
        }
    }
    @IBAction func stepperClecked(_ sender: Any) {
        labelOutlet.isHidden = false
        labelOutlet.text = String(Int(stepperOutlet.value))
    }
    
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //        print("был произведен переход на экран \(segue.identifier!)")
        if segue.identifier == "test1" {
            segue.destination.view.backgroundColor = getColor()
        } else if segue.identifier == "test2" {
            segue.destination.view.backgroundColor = getColor()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelOutlet.isHidden = true
        sliderLabel.isHidden = true
        
    }
    
    func getColor() -> UIColor {
        if segmentControl.selectedSegmentIndex == 0 {
            return .red
        } else if segmentControl.selectedSegmentIndex == 1 {
            return .yellow
        } else if segmentControl.selectedSegmentIndex == 2 {
            return .green
        }
        return .blue
    }
    
    
    
}


