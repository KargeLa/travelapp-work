import UIKit

class GoViewController: UIViewController {
 
    // MARK: - Outlet
    @IBOutlet weak var goTextFieldOutlet: UITextField!
    @IBOutlet weak var goLabelOutlet: UILabel!
    // MARK: - Action
    @IBAction func goButtonClicked(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let backVC = storyBoard.instantiateViewController(withIdentifier: "BackViewController") as? BackViewController
        backVC?.delegate = self
        navigationController?.pushViewController(backVC!, animated: true)
        if let someGoText = goTextFieldOutlet.text {
            backVC?.goText = someGoText
        }
    }
    
    func changeTextOnGoLabel (_ textFromBackTextField: String) {
        goLabelOutlet.text = textFromBackTextField
    }
}
