
import UIKit

class TextButtonViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var textFieldOutlet: UITextField!
    
    @IBAction func buttonClicked(_ sender: UIButton) {
        let storyBoard = UIStoryboard (name: "Main", bundle: nil)
        let controllerFromStoryBoard = storyBoard.instantiateViewController(withIdentifier: "Label_2_ViewController") as? Label_2_ViewController
        navigationController?.pushViewController(controllerFromStoryBoard!, animated: true)
        if let someText = textFieldOutlet.text {
            controllerFromStoryBoard!.text = someText
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
