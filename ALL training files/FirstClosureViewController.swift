import UIKit
class FirstClosureViewController: UIViewController {
    //    var transNewText: ((SecondClosureViewController) -> Void)?
    var text : String = ""
    
    //    MARK: - Outlets
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var firstTextField: UITextField!
    
    //    MARK: - Actions
    
    
    @IBAction func firstButtonClicked(_ sender: Any) {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let controllerFromStoryBoard = storyBoard.instantiateViewController(withIdentifier: "SecondClosureViewController") as? SecondClosureViewController
        controllerFromStoryBoard?.transferFromFirstVC = { text in
            self.firstTextField.text = text
        }
        
        navigationController?.pushViewController(controllerFromStoryBoard!, animated: true)
        if let someText = firstTextField.text {
            controllerFromStoryBoard?.newText = someText
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
}
