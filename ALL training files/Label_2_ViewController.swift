
import UIKit

class Label_2_ViewController: UIViewController {
    //    var delegate : TextButtonViewController?
    var text : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelOutlet.text = text
        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var labelOutlet: UILabel!
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
