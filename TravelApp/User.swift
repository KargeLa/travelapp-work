
import Foundation
class User: CustomStringConvertible {
    var id: Int = 0
    var name: String?
    var username: String?
    var email: String?
    var phone: String?
    var website: String?
    var address: Address?
    var company: Company?
    var description: String {
        return """
        id: \(id)
        name: \(name)
        username: \(username)
        email: \(email)
        phone: \(phone)
        website: \(website)
        address: \(address)
        company: \(address)
        """
    }
}
