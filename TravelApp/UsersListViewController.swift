//
//  UsersListViewController.swift
//  
//
//  Created by Алексей Смоляк on 5/17/19.
//

import UIKit
import  RealmSwift
import Firebase

class UsersListViewController: UIViewController {
    //        MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    var users: [RealmUser] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        users = DataBaseManager.instance.getFromDataBase()
        ApiManager.instance.getUsers { (userFromServer) in
            for user in userFromServer {
                let realmUser = RealmUser()
                realmUser.name = user.name ?? ""
                realmUser.id = user.id
                self.users.append(realmUser)
            }
            DataBaseManager.instance.saveToDataBase(users: self.users)
            self.tableView.reloadData()
        }
    }
    
}



//настройка таблицы
extension UsersListViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for: indexPath) as! UserCell
        let user = users[indexPath.row]
        cell.userNameLabel.text = user.name
        return cell
    }
    
}
