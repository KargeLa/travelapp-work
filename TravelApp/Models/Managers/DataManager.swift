
import Foundation
import  Firebase

class DataManager {
    static var instance = DataManager ()
    var remoteConfig : RemoteConfig?
    
    func fetchFireBaseRemoteConfig(onComplete: @escaping () -> Void) {
        let remoteConfig = RemoteConfig.remoteConfig()
        remoteConfig.fetch { (status, error) in
            remoteConfig.activateFetched()
            self.remoteConfig = remoteConfig
            onComplete()
        }
        
    }
    
    
    
}

