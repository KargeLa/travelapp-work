import Foundation
import RealmSwift



class DataBaseManager {
    static var instance = DataBaseManager()
    func save<T>(_ object: [T]) {
        let realm = try! Realm()
        try! realm.write {
            if let objects = object as? [Object] {
                realm.add(objects, update: true)
            }
        }
    }
    
    func save<T: Object>(_ object: [T]) {
        let realm = try! Realm()
        try! realm.write {
            realm.add(object, update: true)
        }
    }
    
    func getObjects<T: Object>(_ classType: T.Type) -> [T] {
        let realm = try! Realm()
        let result = realm.objects(T.self)
        return Array(result)
    }
    
    func  getTravelsFromDataBase() -> [RealmTravel] {
        let realm = try! Realm()
        let realmTravelsResult = realm.objects(RealmTravel.self)
        return Array(realmTravelsResult)
    }
    
    func getTravelFromDataBase(withId id: String) -> RealmTravel? {
        let realm = try! Realm()
        let travel = realm.object(ofType: RealmTravel.self, forPrimaryKey: id)
        return travel
    }
    func getStopFromDataBase(withId id: String) -> RealmStop? {
        let realm = try! Realm()
        let stop = realm.object(ofType: RealmStop.self, forPrimaryKey: id)
        return stop
    }
    
    func saveToDataBase(travel: [RealmTravel]) { // здесь добавляем в базу данные
        let realm = try! Realm()
        try! realm.write {
            realm.add(travel, update: true)
        }
        
    }
    
    func saveStopToDataBase(stop: [RealmStop]) {
        let realm = try! Realm()
        try! realm.write {
            realm.add(stop, update: true)
        }
    }
    
    func saveToDataBase (users: [RealmUser]) { // здесь добавляем в базу данные
        let realm = try! Realm()
        try! realm.write {
            realm.add(users, update: true)
        }
        
        
    }
    
    func savePostsToDataBase (posts: [RealmPosts]) { // здесь добавляем в базу данные
        let realm = try! Realm()
        try! realm.write {
            realm.add(posts, update: true)
        }
        
        
    }
    
    func savePhotosToDataBase (photos: [RealmPhoto]) { // здесь добавляем в базу данные
        let realm = try! Realm()
        try! realm.write {
            realm.add(photos, update: true)
        }
        
        
    }
    
    func getFromDataBase() -> [RealmUser] {
        let realm = try! Realm()
        let usersResult = realm.objects(RealmUser.self)
        //        usersResult[0]
        var users: [RealmUser] = []
        for realmUser in usersResult {
            users.append(realmUser)
        }
        return users
    }
    
    func getPostsFromDataBase() -> [RealmPosts] {
        let realm = try! Realm()
        let realmPostsResult = realm.objects(RealmPosts.self)
        //        usersResult[0]
        var posts: [RealmPosts] = []
        for realmPost in realmPostsResult {
            posts.append(realmPost)
        }
        return posts
    }
    
    func updateTravel(_ travel: RealmTravel, withName name: String) {
        let realm = try! Realm()
        try! realm.write {
            travel.name = name
        }
    }
    
}



