import  RealmSwift
import Foundation

class RealmPosts : Object {
    @objc var id : Int = 0
    @objc var userId : Int = 0
    @objc var title : String = ""
    @objc var body : String = ""
    @objc var desc: String {
        return """
        id: \(id)
        userId: \(userId)
        title: \(title)
        body: \(body)
        """
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
