import Foundation
class Post: CustomStringConvertible {
    var id : Int = 0
    var userId : Int = 0
    var title : String = ""
    var body : String = ""
    var description: String {
        return """
        id: \(id)
        userId: \(userId)
        title: \(title)
        body: \(body)
        """
    }
}
