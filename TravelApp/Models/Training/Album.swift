import Foundation
class Album: CustomStringConvertible {
    var userId : Int = 0
    var id : Int = 0
    var title : String = ""
    var description: String {
        return """
        userId: \(userId)
        id: \(id)
        title: \(title)
        """
    }
}
