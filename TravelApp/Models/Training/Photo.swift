import Foundation
class Photo: CustomStringConvertible {
    var albumId : Int = 0
    var id : Int = 0
    var title : String = ""
    var url : String = ""
    var thumbnailUrl: String = ""
    var description: String {
        return """
        albumId: \(albumId)
        id: \(id)
        title: \(title)
        url: \(url)
        thumbnailUrl: \(thumbnailUrl)
        """
    }
}
