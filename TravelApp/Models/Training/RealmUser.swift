
import UIKit
import  RealmSwift

class RealmUser: Object {
    @objc dynamic var name: String?
    @objc dynamic var email: String?
    @objc dynamic var id: Int = 0
    @objc dynamic var username: String?
//    @objc dynamic var userName
    
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
