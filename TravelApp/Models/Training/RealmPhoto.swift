import  RealmSwift
import Foundation

class RealmPhoto : Object {
    @objc var albumId : Int = 0
    @objc var id : Int = 0
    @objc var title : String = ""
    @objc var url : String = ""
    @objc var thumbnailUrl: String = ""
    @objc var desc: String {
        return """
        albumId: \(albumId)
        id: \(id)
        title: \(title)
        url: \(url)
        thumbnailUrl: \(thumbnailUrl)
        """
    }
    override static func primaryKey() -> String? {
        return "id"
    }
}
