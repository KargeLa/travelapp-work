import  UIKit
import Foundation
import  RealmSwift

class Travel {

    var name : String = ""
    var desc : String = ""
    var stopsArray : [Stop] = []
    func getAvarageRating() -> Int {
        if stopsArray.count == 0 {
            return 0
        }
        var summ = 0
        for stop in stopsArray {
            summ += stop.rating
        }
        return summ / stopsArray.count
    }
}


