//
//  RealmTravel.swift
//  TravelApp
//
//  Created by Алексей Смоляк on 5/31/19.
//  Copyright © 2019 Алексей Смоляк. All rights reserved.
//

import Foundation
import RealmSwift

class RealmTravel: Object {
    @objc dynamic var name : String = ""
    @objc dynamic var desc : String = ""
    @objc dynamic var id: String = UUID().uuidString //автоматическая генерация строки
    var stopsArray = List<RealmStop>()
    func getAvarageRating() -> Int {
        if stopsArray.count == 0 {
            return 0
        }
        var summ = 0
        for stop in stopsArray {
            summ += stop.rating
        }
        return summ / stopsArray.count
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
