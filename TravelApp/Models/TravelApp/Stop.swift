
import UIKit

class Stop {
    var name: String = ""
    var rating: Int = 0
    var spendMoney: String = "0"
    var currency: String = "$"
    var transportType: Int = 0
    var description: String = ""
    
}
