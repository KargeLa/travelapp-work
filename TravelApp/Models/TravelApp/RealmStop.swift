import RealmSwift
import Foundation

class RealmStop : Object {
    @objc dynamic var id: String = UUID().uuidString
    @objc dynamic var name: String = ""
    @objc dynamic var rating: Int = 0
    @objc dynamic var spendMoney: String = "0"
    @objc dynamic var currency: String = "$"
    @objc dynamic var transportType: Int = 0
    @objc dynamic var desc: String = ""
    @objc dynamic var mapInfo: String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
