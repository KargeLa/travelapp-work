
import UIKit

class AlbumsCell: UITableViewCell {
    //    MARK: - Outlets
    @IBOutlet weak var userIdAlbumLabel: UILabel!
    @IBOutlet weak var idAlbumLabel: UILabel!
    @IBOutlet weak var titleAlbumLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
