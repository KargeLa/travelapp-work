
import UIKit

extension UIView {
    
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
           return layer.cornerRadius //этот код сработае когда в любом месте кто-нибудь обраться к значению
        }
        set { // срабатывает когда собираемся прарвнивать
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
}
