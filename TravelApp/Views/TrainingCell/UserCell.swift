//
//  UserCell.swift
//  TravelApp
//
//  Created by Алексей Смоляк on 5/29/19.
//  Copyright © 2019 Алексей Смоляк. All rights reserved.
//

import UIKit

class UserCell: UITableViewCell {
//    MARK: - Outlets
    @IBOutlet weak var userNameLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
