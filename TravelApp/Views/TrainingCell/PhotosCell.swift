//
//  PhotosCell.swift
//  TravelApp
//
//  Created by Алексей Смоляк on 5/26/19.
//  Copyright © 2019 Алексей Смоляк. All rights reserved.
//

import UIKit

class PhotosCell: UITableViewCell {
    //    MARK: - Outlets
    @IBOutlet weak var albumIdLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var urlImageView: UIImageView!
    @IBOutlet weak var urlImageView2: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
