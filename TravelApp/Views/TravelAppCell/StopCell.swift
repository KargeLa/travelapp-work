import UIKit
import Foundation

class StopCell: UITableViewCell {
    //    MARK : - Outlets
    
    @IBOutlet weak var stopNameLabel: UILabel!
    @IBOutlet weak var descTextLabel: UILabel!
    @IBOutlet weak var moneyLabel: UILabel!
    @IBOutlet var starsColection: [UIImageView]!
    @IBOutlet weak var stopCellView: UIView!
    @IBOutlet weak var transportImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        stopCellView.layer.cornerRadius = 8
        
        // Тут можно настроить закгругления у ячейки
    }
}
