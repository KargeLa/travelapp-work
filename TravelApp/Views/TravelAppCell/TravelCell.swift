
import UIKit

class TravelCell: UITableViewCell {
    //    MARK : - Outlets

    @IBOutlet weak var travelNameLabel: UILabel!
    @IBOutlet weak var descTextLabel: UILabel!
    @IBOutlet var starsColection: [UIImageView]!
    @IBOutlet weak var mainView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        mainView.layer.cornerRadius = 8
        
        // Тут можно настроить закгругления у ячейки
    }
}
