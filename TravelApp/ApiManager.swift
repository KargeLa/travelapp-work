//
//  ApiManage.swift
//  Alamofire
//
//  Created by Алексей Смоляк on 5/17/19.

//  https://jsonplaceholder.typicode.com/users

import Foundation
import Alamofire

//все что объявляется через static var - является синглтоном
class ApiManager {
    static var instance = ApiManager()
    private enum Constants {
        static let baseURL = "https://jsonplaceholder.typicode.com"
    }
    private enum EndPoints {
        static let users = "/users"
        static let posts = "/posts"
        static let photos = "/photos"
        static let albums = "/albums"
    }
    
    func getUsers(onComplete: @escaping ([User]) -> Void) {
        let urlString = Constants.baseURL + EndPoints.users
        AF.request(urlString, method: .get, parameters: [:]) .responseJSON { (response) in
            
            switch response.result {
            case .success(let data):
                print(data)
                if let arrayUsers = data as? Array<Dictionary<String, Any>> {
                    var users = [User]()
                    for userDict in arrayUsers {
                        let user = User()
                        user.id = userDict["id"] as? Int ?? 0
                        user.name = userDict["name"] as? String
                        user.username = userDict["username"] as? String
                        user.email = userDict["email"] as? String
                        user.phone = userDict["phone"] as? String
                        user.website = userDict["website"] as? String
                        users.append(user)
                        if let adressDict = userDict["address"] as? Dictionary<String, Any> {
                            let address = Address()
                            address.street = adressDict["street"] as? String ?? ""
                            address.suite = adressDict["suite"] as? String
                            address.city = adressDict["city"] as? String
                            address.zipcode = adressDict["zipcode"] as? String? ?? ""
                            if let geoDict = adressDict["geo"] as? Dictionary<String, Any> {
                                let geo = Geo()
                                geo.latitude = geoDict["lat"] as? String ?? ""
                                address.geo = geo
                            }
                            user.address = address
                        }
                        if let companyDict = userDict["company"] as? Dictionary<String, Any> {
                            let company = Company()
                            company.name = companyDict["name"] as? String ?? ""
                            company.catchPhrase = companyDict["catchPhrase"] as? String
                            company.bs = companyDict["bs"] as? String
                            user.company = company
                        }
                    }
                    onComplete(users)
                    for user in users {
                        print(user.address?.geo?.latitude)
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func universal(onComplete: @escaping ([User]) -> Void) {
        let urlString = Constants.baseURL + EndPoints.posts
        AF.request(urlString, method: .get, parameters: [:]) .responseJSON { (response) in
            
            switch response.result {
            case .success(let data):
                print(data) //тут хранится ответ от сервера
                if let arrayofSomething = data as? Array<Dictionary<String, Any>> {
                    
                }
                onComplete([])
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getPosts(onComplete: @escaping ([Post]) -> Void) {
        let urlString = Constants.baseURL + EndPoints.posts
        AF.request(urlString, method: .get, parameters: [:]) .responseJSON { (response) in
            
            switch response.result {
            case .success(let data):
                print(data)
                if let arrayPosts = data as? Array<Dictionary<String, Any>> {
                    var posts : [Post] = []
                    for postsDict in arrayPosts {
                        let post = Post()
                        post.id = postsDict["id"] as? Int ?? 0
                        post.userId = postsDict["userId"] as? Int ?? 0
                        post.title = postsDict["title"] as? String ?? ""
                        post.body = postsDict["body"] as? String ?? ""
                        posts.append(post)
                    }
                    onComplete(posts)
                }
                
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
    func getPhotos(onComplete: @escaping ([RealmPhoto]) -> Void) {
        let urlString = Constants.baseURL + EndPoints.photos
        AF.request(urlString, method: .get, parameters: [:]) .responseJSON { (response) in
            
            switch response.result {
            case .success(let data):
                print(data) //тут хранится ответ от сервера
                if let arrayPhotos = data as? Array<Dictionary<String, Any>> {
                    var photos : [RealmPhoto] = []
                    for photoDict in arrayPhotos {
                        let photo = RealmPhoto()
                        photo.albumId = photoDict["albumId"] as? Int ?? 0
                        photo.id = photoDict["id"] as? Int ?? 0
                        photo.title = photoDict["title"] as? String ?? ""
                        photo.url = photoDict["url"] as? String ?? ""
                        photo.thumbnailUrl = photoDict["thumbnailUrl"] as? String ?? ""
                        photos.append(photo)
                    }
                    onComplete(photos)
                }
                
                
            case .failure(let error):
                print(error)
            }
        }
    }
    func getAlbums(onComplete: @escaping ([Album]) -> Void) {
        let urlString = Constants.baseURL + EndPoints.albums
        AF.request(urlString, method: .get, parameters: [:]) .responseJSON { (response) in
            
            switch response.result {
            case .success(let data):
                print(data) //тут хранится ответ от сервера
                if let arrayAlbums = data as? Array<Dictionary<String, Any>> {
                    var albums : [Album] = []
                    for albumDict in arrayAlbums {
                        let album = Album()
                        album.userId = albumDict["userId"] as? Int ?? 0
                        album.id = albumDict["id"] as? Int ?? 0
                        album.title = albumDict["title"] as? String ?? ""
                        albums.append(album)
                    }
                    onComplete(albums)
                }
                
                
            case .failure(let error):
                print(error)
            }
        }
    }
}
