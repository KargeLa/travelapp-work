
import UIKit
import MapKit


protocol MapViewControllerDelegate {
    func mapControllerDidSelectPoint(_ point: MKPointAnnotation)
}


class MapViewController: UIViewController {
    
    
    //MARK: - Properties

    var delegate: MapViewControllerDelegate?
    
    
    //MARK:- Outlets
    
    
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var saveButtonClicked: UIButton!
    
    
    //MARK: - Actions
    
    @IBAction func mapClicked(_ recognizer : UITapGestureRecognizer) {
        let point = recognizer.location(in: mapView)
        let tapPoint = mapView.convert(point, toCoordinateFrom: mapView)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = tapPoint
        
        mapView.removeAnnotations(mapView.annotations)
        mapView.addAnnotation(annotation)
    }
    
    @IBAction func saveClicked(_ sender: Any) {
        if let selectedPoint = mapView.annotations.first as? MKPointAnnotation {
            delegate?.mapControllerDidSelectPoint(selectedPoint)
        }
        navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- Lifecycle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

//CreateStopViewController


//а это в
