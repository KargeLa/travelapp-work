
import UIKit
import  RealmSwift

class StopsListViewController: UIViewController {
    var travel: RealmTravel!
    
    //    MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    //    MARK: - Actions
    @IBAction func addButtonClicked(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let createStopVC = storyBoard.instantiateViewController(withIdentifier: "CreateStopViewController") as! CreateStopViewController
        //        createStopVC.stopDidCreatClosure = { stop in
        //            self.travel.stopsArray.append(stop)
        createStopVC.delegate = self
        navigationController?.pushViewController(createStopVC, animated: true)
    }
    
    
    
    
    
    func createControllerDidCreateStop(_ stop: RealmStop) {
//        travel.stopsArray.append(stop)
//        tableView.reloadData()
        let realm = try! Realm()
        try! realm.write { // открываю транзакцию для изменния в базе данных
            travel.stopsArray.append(stop)
        }
        tableView.reloadData()
    }
    
    //MARK: - LifeCircle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = travel.name
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension StopsListViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return travel.stopsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! StopCell
        let shadowPath2 = UIBezierPath(rect: cell.bounds)
        cell.layer.masksToBounds = false
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width: CGFloat(1.0), height: CGFloat(3.0))
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowPath = shadowPath2.cgPath
        let stop = travel.stopsArray[indexPath.row]
        print("stop")
        cell.stopNameLabel.text = stop.name
        cell.descTextLabel.numberOfLines = 0
        cell.descTextLabel.text = stop.desc
        cell.moneyLabel.text = stop.spendMoney
        for i in 0..<stop.rating {
            let star = cell.starsColection[i]
            star.isHighlighted = true
        }
        if stop.transportType == 0 {
            cell.transportImageView.image = UIImage.init(named: "icon_plan")
        } else if stop.transportType == 1 {
            cell.transportImageView.image = UIImage.init(named: "icon_car")
        } else if stop.transportType == 2 {
            cell.transportImageView.image = UIImage.init(named: "icon_train")
        }
        return cell
        
    }
    //    здесь обрабатывается нажатие на ячейку
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let stroryBoard = UIStoryboard(name: "Main", bundle: nil)
        let stopListVC = stroryBoard.instantiateViewController(withIdentifier: "CreateStopViewController") as! CreateStopViewController
        stopListVC.stop = travel.stopsArray[indexPath.row]
        navigationController?.pushViewController(stopListVC, animated: true)
    }
}
