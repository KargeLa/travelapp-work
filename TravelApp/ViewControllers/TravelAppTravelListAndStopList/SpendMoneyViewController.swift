
import UIKit

class SpendMoneyViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    //    MARK: - Outlets
    @IBOutlet weak var spendMoneytextField: UITextField!
    @IBOutlet weak var segmentedControlOutlet: UISegmentedControl!
    //     MARK: - Properties
    var delegate : CreateStopViewController?
    //    MARK: - Actions
    
    @IBAction func redyClicked(_ sender: UIButton) {
        if segmentedControlOutlet.selectedSegmentIndex == 0 {
            if let text = spendMoneytextField.text {
                delegate?.stop.spendMoney = text + " $"
                delegate?.userSpentMoney(text, currency: "$")
            }
        } else if segmentedControlOutlet.selectedSegmentIndex == 1 {
            if let text = spendMoneytextField.text {
                delegate?.stop.spendMoney = text + " €"
                delegate?.userSpentMoney(text, currency: "€")
            }
        } else if segmentedControlOutlet.selectedSegmentIndex == 2 {
            if let text = spendMoneytextField.text {
                delegate?.stop.spendMoney = text + " ₽"
                delegate?.userSpentMoney(text, currency: "₽")
            }
        }
        navigationController?.popViewController(animated: true)
    }
    
}




