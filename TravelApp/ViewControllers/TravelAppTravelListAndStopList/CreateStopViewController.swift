import UIKit
import MapKit
import RealmSwift

class CreateStopViewController: UIViewController {
    //MARK: - Properties
    var stop = RealmStop()
    var delegate : StopsListViewController?
//    var stopDidCreatClosure: ((Stop) -> Void)?
    
    //MARK: - Lifecicle
    override func viewDidLoad() {
        super.viewDidLoad()
        nameTextField.text = stop.name
        
        
    }
    //    MARK: - Actions
    @IBAction func chouseTrasportSegmentedControllerClecked(_ sender: UISegmentedControl) {
        stop.transportType = sender.selectedSegmentIndex
    }
    
    @IBAction func reitingStepperClicked(_ sender: UIStepper) {
        reitingLabel.text = String(Int(reitingOutlet.value))
    }
    
    @IBAction func saveButtonClicked(_ sender: Any) {
        print ("test")
        let realm = try! Realm()
        realm.beginWrite()
        if let name = nameTextField.text {
            stop.name = name
            print ("name\(name)")
            print ("stop.name\(stop.name)")
            
            
        }
        if let raiting = reitingLabel.text, let intRaiting = Int(raiting) {
            stop.rating = intRaiting
        }
        if let desc = descriptionTextViewOutlet.text {
            stop.desc = desc
        }
        if let mapInfo = mapLabel.text {
            stop.mapInfo = mapInfo
        }
        try! realm.commitWrite()
        delegate?.createControllerDidCreateStop(stop)
        
//        stopDidCreatClosure?(stop)
        navigationController?.popViewController(animated: true)
    }
    @IBAction func chooseValueButtonClicked(_ sender: UIButton) {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let controllerFromStoryBoard = storyBoard.instantiateViewController(withIdentifier: "SpendMoneyViewController") as? SpendMoneyViewController
        controllerFromStoryBoard?.delegate = self
        navigationController?.pushViewController(controllerFromStoryBoard!, animated: true)
    }
    @IBAction func mapButtonClicked(_ sender: UIButton) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let mapVC = storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        mapVC.delegate = self
        navigationController?.pushViewController(mapVC, animated: true)
    }
    
    //    MARK: - Outlets
    @IBOutlet weak var moneyCountOutlet: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var reitingOutlet: UIStepper!
    
   
    @IBOutlet weak var mapLabel: UILabel!
    @IBOutlet weak var reitingLabel: UILabel!
    @IBOutlet weak var descriptionTextViewOutlet: UITextView!
    
    
    //MARK: - Public
    func userSpentMoney(_ moneyCount: String, currency: String) {
        print("Я потратил \(moneyCount)")
        moneyCountOutlet.text = moneyCount + currency
    }
}

extension CreateStopViewController: MapViewControllerDelegate {
    func mapControllerDidSelectPoint(_ point: MKPointAnnotation) {
        mapLabel.text = "\(point.coordinate.latitude) - \(point.coordinate.longitude)"
//        mapLabel.lineBreakMode = .byWordWrapping
//        mapLabel.numberOfLines = 0

    }
}
