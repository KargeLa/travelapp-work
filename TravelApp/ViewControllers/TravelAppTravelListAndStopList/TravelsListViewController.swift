
import UIKit
import  RealmSwift

class TravelsListViewController: UIViewController {
    var travelsArray : [RealmTravel] = []
    
    //    MARK: - Outlets
    @IBOutlet weak var tableViewOutlet: UITableView!
    
    //    MARK: - Lifecicle
    
//    экран показан, но без верстки
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewOutlet.delegate = self
        tableViewOutlet.dataSource = self
        travelsArray = DataBaseManager.instance.getObjects(RealmTravel.self)
        navigationController?.isNavigationBarHidden = false
        self.navigationItem.setHidesBackButton(true, animated: true)
    }
//    почти показали
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableViewOutlet.reloadData()
    }
////      показаил и уже есть верскта
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//    }
////    срабатывает за долю секунды до того как изчезнет
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//    }
////    срабатывает сразу после того как экран исчез
//    override func viewDidDisappear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//    }
//    //    срабатывает когда экран освобождается из памяти(нолностью удален)
//    deinit {
//        
//    }
////
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//    }
//    //    срабатывает когда переворачиваем телефон в ландшафтный вариант положения экрана
//    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
//        super.viewWillTransition(to: size, with: coordinator)
//    }
////    срабатывает когда происходит перелимит по использованию оперативной памяти
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//    }
    
    //    MARK: - Actions
    @IBAction func addButtonClicked(_ sender: Any) {
        let alertController = UIAlertController(title: "Введите название старны", message: nil, preferredStyle: .alert)
        
        // ADD ACTIONS HANDLER
        let loginAction = UIAlertAction(title: "Сохранить", style: .default) { (_) in
            
            // тут обрабатывается нажатие на кнопку сохранить
            let travelNameTextField = alertController.textFields![0] as UITextField
            let travelDescriptionTextField = alertController.textFields![1] as UITextField
            
           
            
            let realmTravel = RealmTravel()
            realmTravel.name = travelNameTextField.text ?? ""
            realmTravel.desc = travelDescriptionTextField.text ?? ""
            
            DataBaseManager.instance.save([realmTravel])
            
            self.travelsArray.append(realmTravel)
            self.tableViewOutlet.reloadData()
        }
        loginAction.isEnabled = false
        alertController.addAction(loginAction)
        
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel) { (_) in
            // do something
        }
        alertController.addAction(cancelAction)
        
        // ADD TEXT FIELDS
        alertController.addTextField { (textField) in
            textField.placeholder = "Название"
        }
        alertController.addTextField { (textField) in
            textField.placeholder = "Описание"
            
            NotificationCenter.default.addObserver(forName: UITextField.textDidChangeNotification, object: textField, queue: OperationQueue.main) { (notification) in
                loginAction.isEnabled = textField.text != ""
            }
        }
        
        // PRESENT
        present(alertController, animated: true)
    }
    
    
}
//настройка таблицы
extension TravelsListViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return travelsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! TravelCell
        let travel = travelsArray[indexPath.row]
        cell.travelNameLabel.text = travel.name
        let shadowPath2 = UIBezierPath(rect: cell.bounds)
        cell.layer.masksToBounds = false
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width: CGFloat(1.0), height: CGFloat(3.0))
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowPath = shadowPath2.cgPath
        cell.descTextLabel.text = travel.desc
        let averageRating = travel.getAvarageRating()
        for i in 0..<averageRating {
            let star = cell.starsColection[i]
            star.isHighlighted = true
        }
        return cell
    }
//    здесь обрабатывается нажатие на ячейку
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let stroryBoard = UIStoryboard(name: "Main", bundle: nil)
        let stopListVC = stroryBoard.instantiateViewController(withIdentifier: "StopsListViewController") as! StopsListViewController
        let travel = travelsArray[indexPath.row]
        stopListVC.travel = travel
        navigationController?.pushViewController(stopListVC, animated: true)
    }
}
//Если в таблице не отборажаются ячейки первым делом нужно проверить установлен ли delegate и datasource
