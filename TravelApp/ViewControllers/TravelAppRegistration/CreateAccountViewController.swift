
import UIKit
import Firebase

class CreateAccountViewController: UIViewController {
    let loginInf : Dictionary<String, String> = [:]

    //    MARK: - Outlets
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    
    //    MARK: - Actions
    @IBAction func backButtonClicked(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func createClicked(_ sender: Any) {
        if let login = emailTextField.text, let password = passwordTextField.text {
            Auth.auth().createUser(withEmail: login, password: password) { (result, error) in
                if error == nil {
                    
                    self.errorLabel.textColor = .green
                    self.passwordLabel.textColor = .green
                    self.emailLabel.textColor = .green
                    self.errorLabel.text = "successful"
                    
                    self.navigationController?.popViewController(animated: true)
                    
                } else {
                    
                    switch error.unsafelyUnwrapped.localizedDescription {
                        
                    case "The email address is already in use by another account.":
                        self.emailLabel.textColor = .red
                        self.emailLabel.text = "The email address is already in use by another account."
                        
                    case "The password must be 6 characters long or more.":
                        self.passwordLabel.textColor = .red
                        self.passwordLabel.text = "The password must be 6 characters long or more"
                        
                    case "An email address must be provided.":
                        self.emailLabel.textColor = .red
                        self.emailLabel.text = "Uncorrect type of email address"
                        
                    case "The email address is badly formatted.":
                        self.emailLabel.textColor = .red
                        self.emailLabel.text = "Uncorrect type of email address"
                        
                    default:
                        print(error.unsafelyUnwrapped.localizedDescription)
                        self.errorLabel.textColor = .red
                        self.errorLabel.text = "error"
                    }
                }
            }
        }
    }
    
    //    MARK: - LifeCircle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
}
