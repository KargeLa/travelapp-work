
import UIKit
import  Firebase
class WelcomeViewController: UIViewController {
    
    //    MARK: - Outlets
    @IBOutlet weak var imageViewOutlet: UIImageView!
    @IBOutlet weak var moreWaysToLoginButton: UIButton!
    //        MARK: - Actions
    
    @IBAction func reserveButtonClicked(_ sender: Any) {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let reserveVC = storyBoard.instantiateViewController(withIdentifier: "TravelsListViewController") as! TravelsListViewController
        navigationController?.pushViewController(reserveVC, animated: true)
    }
    
    @IBAction func createButtonClicked(_ sender: Any) {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let createVC = storyBoard.instantiateViewController(withIdentifier: "CreateAccountViewController") as! CreateAccountViewController
        navigationController?.pushViewController(createVC, animated: true)
    }
    
    @IBAction func loginWithEmailButtonClicked(_ sender: Any) {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let loginWithEmailVC = storyBoard.instantiateViewController(withIdentifier: "LoginWithEmailViewController") as! LoginWithEmailViewController
        navigationController?.pushViewController(loginWithEmailVC, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true

        if let buttontitle = DataManager.instance.remoteConfig?["button_title"].stringValue {
            self.moreWaysToLoginButton.setTitle(buttontitle, for: .normal)
        }

        if let isButtonHidden = DataManager.instance.remoteConfig?["button_hidden"].boolValue {
            self.moreWaysToLoginButton.isHidden = isButtonHidden
        }
    }
    

//  
//    
//    @IBAction func createClicked(_ sender: Any) {
//        Auth.auth().createUser(withEmail: "2@test.com", password: "111111") { (result, error) in
//            if error == nil {
//                let user = result?.user
//                print(result?.user.email)
//            } else {
//                print("не смогли зарегистрировать")
//            }
//            //            Auth.auth().
//        }
//        //        let createVC = controller(withID: "sdfasdad")
//        //        navigationController?.pushViewController(createVC , animated: true)
//    }
    
}
