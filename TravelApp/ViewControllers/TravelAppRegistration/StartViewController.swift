
import UIKit

class StartViewController: UIViewController {
    
    //    MARK: - Outlet
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DataManager.instance.fetchFireBaseRemoteConfig(onComplete: { [weak self] in
            let welcomVC = self?.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
            self?.navigationController?.pushViewController(welcomVC, animated: true)
        })
        // Do any additional setup after loading the view.
    }
    

}
