//
//  ForgotPasswordViewController.swift
//  TravelApp
//
//  Created by Алексей Смоляк on 4/18/19.
//  Copyright © 2019 Алексей Смоляк. All rights reserved.
//
import Firebase
import UIKit

class ForgotPasswordViewController: UIViewController {
    //    MARK: - Outlets
    @IBOutlet weak var emailTextField: UITextField!
    
    //    MARK: - Actions
    @IBAction func resetButtonClicked(_ sender: Any) {
        if let email = emailTextField.text {
            Auth.auth().sendPasswordReset(withEmail: email) { (error) in
                if error == nil {
                    print("новый пароль отпрвлен успешно")
                } else {
                    print("новый пароль не отправлен!")
                }
            }
        }
        
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
}
