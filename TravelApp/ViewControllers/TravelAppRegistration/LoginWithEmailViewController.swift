//
//  LoginWithEmailViewController.swift
//  TravelApp
//
//  Created by Алексей Смоляк on 4/12/19.
//  Copyright © 2019 Алексей Смоляк. All rights reserved.
//

import UIKit
import Firebase

class LoginWithEmailViewController: UIViewController {
    
    //MARK: - Ouotlets
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var isShow: UIButton!
    @IBOutlet weak var isntShow: UIButton!
    @IBOutlet weak var miniView: UIView!
    
    //MARK: - Actions
    @IBAction func secureButtonActionIsHidden(_ sender: Any) {
        passwordTextField.isSecureTextEntry = false
        isShow.isHidden = true
        isntShow.isHidden = false
    }
    @IBAction func secureButtonActionNotHidden(_ sender: Any) {
        passwordTextField.isSecureTextEntry = true
        isShow.isHidden = false
        isntShow.isHidden = true
    }
    
    @IBAction func forgotButtonClicked(_ sender: Any) {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let forgotPassVC = storyBoard.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        navigationController?.pushViewController(forgotPassVC, animated: true)
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func getStartedClicked(_ sender: Any) {
        if let email = emailTextField.text, let password = passwordTextField.text {
            
            Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
                if error == nil {
                    let user = result?.user
                    print(user?.email)
                    let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
                    let travelListVC = storyBoard.instantiateViewController(withIdentifier: "TravelsListViewController") as! TravelsListViewController
                   self.navigationController?.pushViewController(travelListVC, animated: true)
                } else {
                    print("ошибка логина")
                    self.miniView.backgroundColor = .red
                    self.passwordTextField.textColor = .red
                }
            }
            
        }
    }
    
    //MARK: - LifeCircle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    
}
