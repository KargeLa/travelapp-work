import RealmSwift
import UIKit

class RealmNotificationsExampleViewController: UIViewController {
    //    MARK: - Properties
    var travel: RealmTravel?
    var notificationToken: NotificationToken? = nil
    
    //    MARK: - Outlets
    @IBOutlet weak var resultLabel: UILabel!
    //    MARK: - Actions
    
    @IBAction func createTravel(_ sender: Any) {
        travel = RealmTravel()
        travel?.id = "XXX"
        travel?.name = "Alexey"
        DataBaseManager.instance.saveToDataBase(travel: [travel!])
        notificationToken = travel?.observe({ (change) in
            switch change {
            case .change(let properties):
                for property in properties {
                    if property.name == "name" {
                        self.resultLabel.text = property.newValue as? String
                        print("Congratulations, you've exceeded 1000 steps.")
                    }
                }
            case .error(let error):
                print("An error occurred: \(error)")
            case .deleted:
                print("The object was deleted.")
                
            }
        })
    }
    //    MARK: - Lifecicle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
}
