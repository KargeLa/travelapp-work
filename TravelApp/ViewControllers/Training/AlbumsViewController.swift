
import UIKit

class AlbumsViewController: UIViewController {
    var albumArray : [Album] = []
    //    MARK: - Outlets
    @IBOutlet weak var albumTabelVIew: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ApiManager.instance.getAlbums { (albumsFromServer/* название задаю сам */) in
            self.albumArray = albumsFromServer
            self.albumTabelVIew.reloadData()
            
            // Do any additional setup after loading the view.
        }
        
        
        /*
         // MARK: - Navigation
         
         // In a storyboard-based application, you will often want to do a little preparation before navigation
         override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         // Get the new view controller using segue.destination.
         // Pass the selected object to the new view controller.
         }
         */
        
    }
}


//настройка таблицы
extension AlbumsViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return albumArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AlbumsCell", for: indexPath) as! AlbumsCell
        let album = albumArray[indexPath.row]
        cell.idAlbumLabel.text = String(album.id)
        cell.userIdAlbumLabel.text = String(album.userId)
        cell.titleAlbumLabel.text = album.title
        
        return cell
    }
    
}
