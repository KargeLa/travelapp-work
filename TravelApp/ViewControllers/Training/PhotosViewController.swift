//
//  PhotosViewController.swift
//  TravelApp
//
//  Created by Алексей Смоляк on 5/26/19.
//  Copyright © 2019 Алексей Смоляк. All rights reserved.
//

import UIKit
import Kingfisher
import RealmSwift
import  Foundation

class PhotosViewController: UIViewController {
    var photosArray : [RealmPhoto] = []
    
    //    MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ApiManager.instance.getPhotos { (photosFromServer/* название задаю сам */) in
            
            for photo in photosFromServer {
                let realmPhoto = RealmPhoto()
                
                realmPhoto.albumId = photo.albumId
                realmPhoto.id = photo.id
                realmPhoto.title = photo.title
                realmPhoto.url = photo.url
                self.photosArray.append(realmPhoto)
            }
            DataBaseManager.instance.savePhotosToDataBase(photos: self.photosArray)
            self.tableView.reloadData()
        }
        ApiManager.instance.getPhotos { (photosFromServer/* название задаю сам */) in
            //            self.realmPostsArray = postsFromServer
            self.tableView.reloadData()
        }
            }
    
    
    // Do any additional setup after loading the view.
}





//настройка таблицы
extension PhotosViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return photosArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PhotosCell", for: indexPath) as! PhotosCell
        let photo = photosArray[indexPath.row]
        cell.albumIdLabel.text = String(photo.albumId)
        cell.idLabel.text = String(photo.id)
        cell.titleLabel.text = photo.title
        if let url = URL(string: photo.url) {
            cell.urlImageView.kf.setImage(with: url)
        }
        if let url = URL(string: photo.thumbnailUrl) {
            cell.urlImageView2.kf.setImage(with: url)
        }
        
        return cell
    }
    
}
