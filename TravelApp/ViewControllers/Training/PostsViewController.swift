
import UIKit

class PostsViewController: UIViewController {
    var realmPostsArray : [RealmPosts] = []
    
    //    MARK : - Outlets
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        realmPostsArray = DataBaseManager.instance.getPostsFromDataBase()
        ApiManager.instance.getPosts { (postFromServer) in
            for post in postFromServer {
                let realmPost = RealmPosts()
                realmPost.body = post.body ?? ""
                realmPost.id = post.id
                realmPost.title = post.title
                realmPost.userId = post.userId
                self.realmPostsArray.append(realmPost)
            }
            DataBaseManager.instance.savePostsToDataBase(posts: self.realmPostsArray)
            self.tableView.reloadData()
        }
        ApiManager.instance.getPosts { (postsFromServer/* название задаю сам */) in
//            self.realmPostsArray = postsFromServer
            self.tableView.reloadData()
        }

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}



//настройка таблицы
extension PostsViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return realmPostsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell", for: indexPath) as! PostCell
        let post = realmPostsArray[indexPath.row]
        cell.idLabel.text = String(post.id)
        cell.userIdLabel.text = String(post.userId)
        cell.titleLabel.text = post.title
        cell.bodyLabel.text = post.body

        return cell
    }

}
