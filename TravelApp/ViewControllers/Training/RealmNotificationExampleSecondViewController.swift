import UIKit
import RealmSwift

class RealmNotificationExampleSecondViewController: UIViewController {
    
    //    MARK: - Properties
    var travel: RealmTravel?
    
    //    MARK: - Outlets
    @IBOutlet weak var textField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        travel = DataBaseManager.instance.getTravelFromDataBase(withId: "XXX")
        
    }
    
    @IBAction func textFieldDidChange(_ sender: Any) {
        let realm = try! Realm()
        try! realm.write {
            self.travel?.name = textField.text!
        }
        //        DataBaseManager.instance.updateTravel(travel!, withName: textField.text!)
    }
    
    
}

extension RealmNotificationExampleSecondViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        
    }
}



